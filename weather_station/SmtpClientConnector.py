'''
Created on Jan 26, 2019

@author: Kedar Patil
'''

import smtplib
import ConfigUtil
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class SmtpClientConnector(object):
    
    def __init__(self):
        
        ''' 
        initializing the ConfigUtil class by calling its constructor
        ''' 
        
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig() #loading configurations
        print('Configuration data: ' + str(self.config.getConfigData(False)))
    
    '''
    Creating a publishMessage method to set the details for sending an Email
    so that this method could be called in the TempSensorEmulator to send an alert
    '''
                 
    def publishMessage(self, topic, data):
        host = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.HOST_KEY)
        port = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.PORT_KEY)
        fromAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.TO_ADDRESS_KEY)
        authToken = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.USER_AUTH_TOKEN_KEY)
        
        '''
        setting the message details such as: from, to and subject 
        to send the Email alert
        '''
        
        msg = MIMEMultipart()
        msg['from'] = fromAddr
        msg['to'] = toAddr
        msg['subject'] = topic
        msgBody = str(data)
        msg.attach(MIMEText(msgBody))
        msgText = msg.as_string()
        
        '''
        send Email alert
        '''
        
        smtpServer = smtplib.SMTP_SSL(host, port)
        smtpServer.ehlo()
        smtpServer.login(fromAddr, authToken)
        smtpServer.sendmail(fromAddr, toAddr, msgText)
        smtpServer.close()