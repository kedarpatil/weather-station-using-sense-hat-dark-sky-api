'''
Created on Apr 19, 2019

@author: Kedar Patil
'''

import urllib.request, urllib.error, urllib.parse
import json
import os
from distutils.util import strtobool
import subprocess
import glob
import time
from time import sleep
import RPi.GPIO as io
import SmtpClientConnector
import DataUtil
import SensorData
from ISStreamer.Streamer import Streamer
from sense_hat import SenseHat

# --------- User Settings ---------
CITY = "Boston"
GPS_COORDS = "42.3601,-71.0589"
SENSOR_LOCATION_NAME = "Home"
DARKSKY_API_KEY = "367fc6d1d856df4148d2871c043e7053"
BUCKET_NAME = ":partly_sunny: " + CITY + " Weather"
BUCKET_KEY = "shds1"
ACCESS_KEY = "ist_tcTd_j7QqZ6VlV-pW3i-ENyprbcZlM-A" #initialstate
MINUTES_BETWEEN_READS = 0.10
METRIC_UNITS = True
# ---------------------------------

def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def get_current_conditions():
    api_conditions_url = "https://api.darksky.net/forecast/" + DARKSKY_API_KEY + "/" + GPS_COORDS + "?units=auto"
    try:
        f = urllib.request.urlopen(api_conditions_url)
    except:
        return []
    json_currently = f.read()
    f.close()
    return json.loads(json_currently.decode('utf-8'))

def moon_icon(moon_phase):
    if moon_phase == 0:
        return ":new_moon:"
    if moon_phase < .125:
        return ":waxing_crescent_moon:"
    if moon_phase < .25:
        return ":first_quarter_moon:"
    if moon_phase < .48:
        return ":waxing_gibbous_moon:"
    if moon_phase < .52:
        return ":full_moon:"
    if moon_phase < .625:
        return ":waning_gibbous_moon:"
    if moon_phase < .75:
        return ":last_quarter_moon:"
    if moon_phase < 1:
        return ":waning_crescent_moon:"
    return ":crescent_moon:"

def weather_icon(ds_icon):
    icon = {
        "clear-day"                : ":sunny:",
        "clear-night"           : ":new_moon_with_face:",
        "rain"                  : ":umbrella:",
        "snow"                  : ":snowflake:",
        "sleet"                 : ":sweat_drops: :snowflake:",
        "wind"                  : ":wind_blowing_face:",
        "fog"                   : ":fog:",
        "cloudy"                : ":cloud:",
        "partly-cloudy-day"     : ":partly_sunny:",
        "partly-cloudy-night"   : ":new_moon_with_face:",
        "unknown"               : ":sun_with_face:",
    }
    return icon.get(ds_icon,":sun_with_face:")

def weather_status_icon(ds_icon,moon_phase):
    icon = weather_icon(ds_icon)
    if (icon == ":new_moon_with_face:"):
        return moon_icon(moon_phase)
    return icon

def wind_dir_icon(wind_bearing):
    if (wind_bearing < 20):
        return ":arrow_up:"
    if (wind_bearing < 70):
        return ":arrow_upper_right:"
    if (wind_bearing < 110):
        return ":arrow_right:"
    if (wind_bearing < 160):
        return ":arrow_lower_right:"
    if (wind_bearing < 200):
        return ":arrow_down:"
    if (wind_bearing < 250):
        return ":arrow_lower_left:"
    if (wind_bearing < 290):
        return ":arrow_left:"
    if (wind_bearing < 340):
        return ":arrow_upper_left:"
    return ":arrow_up:"

def main():
    sense = SenseHat()
    red = (255, 0, 0)
    blue = (0, 255, 255)
    orange = (255, 165, 0)
    datautil = DataUtil.DataUtil()
    sensorData = SensorData.SensorData()
    
    proc = subprocess.Popen(["node-red-start", "/home/pi/iot-project/weather_station/flows.json"], stdout=subprocess.PIPE, shell=True)
    smtpconnect = SmtpClientConnector.SmtpClientConnector() #SmtpClientConnector initialization
    
    curr_conditions = get_current_conditions()
    if ('currently' not in curr_conditions):
        print("Error! Dark Sky API call failed, check your GPS coordinates and make sure your Dark Sky API key is valid!\n")
        print(curr_conditions)
        exit()
    else:
        streamer = Streamer(bucket_name=BUCKET_NAME, bucket_key=BUCKET_KEY, access_key=ACCESS_KEY)
    while True:
        # -------------- Sense Hat --------------
        # Read the sensors
        temp_c = sense.get_temperature()
        humidity = sense.get_humidity()
        pressure_mb = sense.get_pressure()
        
        #----------------ALERT-------------------
        sense.clear ()
        # Get the data from the file
        with open('ubiVal.txt', 'rb') as fp:
            v = (strtobool(fp.read().decode('utf-8')))
            if (v == 1):
                while True:
                    sense.show_letter("O", red)
                    sleep(1)
                    sense.show_letter("N", red)
                    sleep(1)
                    sense.clear ()
                    break
            else:
                while True:
                    sense.show_letter("O", blue)
                    sleep(1)
                    sense.show_letter("F", blue)
                    sleep(1)
                    sense.clear ()
                    sleep(1)
                    sense.show_letter("F", blue)
                    sleep(1)
                    sense.clear ()
                    sleep(1)
                    break
                    
        
        #-------------JSON for Ubidots-----
        jsontd = json.dumps(round(temp_c, 3))
        outputtd = open('TempData.txt', 'w')
        outputtd.write(jsontd)

        jsonhd = json.dumps(round(humidity, 3))
        outputhd = open('HumidityData.txt', 'w')
        outputhd.write(jsonhd)

        jsonpd = json.dumps(pressure_mb)
        outputpd = open('PressureData.txt', 'w')
        outputpd.write(jsonpd)

        # Format the data
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        temp_f = float("{0:.2f}".format(temp_f))
        temp_c = float("{0:.2f}".format(temp_c))
        humidity = float("{0:.2f}".format(humidity))
        pressure_in = 0.0295301*(pressure_mb)
        pressure_in = float("{0:.2f}".format(pressure_in))
        pressure_mb = float("{0:.2f}".format(pressure_mb))

        # SMTP Email
        if (sensorData.getAvgValue() > 48):
            sensorData.addVal(temp_c)
            smtpconnect.publishMessage('Temperature Alert', datautil.SensordataToJson(sensorData))
            print('\nTemperature exceeds the threshold...\nSending an Email alert to the user...')
            sense.show_letter("H", orange)
            sleep(2)
            sense.clear()
            sleep(1)
        if (sensorData.getAvgValue() < 20):
            sensorData.addVal(temp_c)
            smtpconnect.publishMessage('Temperature Alert', datautil.SensordataToJson(sensorData))
            print('\nTemperature is lower than the threshold...\nSending an Email alert to the user...')
            sense.show_letter("C", blue)
            sleep(2)
            sense.clear()
            sleep(1)
        
        # Print and stream
        if (METRIC_UNITS):
            print(SENSOR_LOCATION_NAME + " Temperature(C): " + str(temp_c))
            print(SENSOR_LOCATION_NAME + " Pressure(mb): " + str(pressure_mb))
            streamer.log(":sunny: " + SENSOR_LOCATION_NAME + " Temperature(C)", temp_c)
            streamer.log(":cloud: " + SENSOR_LOCATION_NAME + " Pressure (mb)", pressure_mb)
        else:
            print(SENSOR_LOCATION_NAME + " Temperature(F): " + str(temp_f))
            print(SENSOR_LOCATION_NAME + " Pressure(IN): " + str(pressure_in))
            streamer.log(":sunny: " + SENSOR_LOCATION_NAME + " Temperature(F)", temp_f)
            streamer.log(":cloud: " + SENSOR_LOCATION_NAME + " Pressure (IN)", pressure_in)
        print(SENSOR_LOCATION_NAME + " Humidity(%): " + str(humidity))
        streamer.log(":sweat_drops: " + SENSOR_LOCATION_NAME + " Humidity(%)", humidity)

        # -------------- Dark Sky --------------
        curr_conditions = get_current_conditions()
        if ('currently' not in curr_conditions):
            print("Error! Dark Sky API call failed. Skipping a reading then continuing ...\n")
            print(curr_conditions)
        else:
            streamer.log(":house: Location",GPS_COORDS)

            if 'humidity' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['humidity']):
                streamer.log(":droplet: Humidity(%)", curr_conditions['currently']['humidity']*100)

            if 'temperature' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['temperature']):
                streamer.log("Temperature",curr_conditions['currently']['temperature'])

            if 'apparentTemperature' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['apparentTemperature']):
                streamer.log("Feels Like",curr_conditions['currently']['apparentTemperature'])

            if 'dewPoint' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['dewPoint']):
                streamer.log("Dewpoint",curr_conditions['currently']['dewPoint'])

            if 'windSpeed' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['windSpeed']):
                streamer.log(":dash: Wind Speed",curr_conditions['currently']['windSpeed'])

            if 'windGust' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['windGust']):
                streamer.log(":dash: Wind Gust",curr_conditions['currently']['windGust'])

            if 'windBearing' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['windBearing']):
                streamer.log(":dash: Wind Direction",wind_dir_icon(curr_conditions['currently']['windBearing']))

            if 'pressure' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['pressure']):
                streamer.log("Pressure",curr_conditions['currently']['pressure'])

            if 'precipIntensity' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['precipIntensity']):
                streamer.log(":umbrella: Precipitation Intensity",curr_conditions['currently']['precipIntensity'])

            if 'precipProbability' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['precipProbability']):
                streamer.log(":umbrella: Precipitation Probabiity(%)",curr_conditions['currently']['precipProbability']*100)

            if 'cloudCover' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['cloudCover']):
                streamer.log(":cloud: Cloud Cover(%)",curr_conditions['currently']['cloudCover']*100)

            if 'uvIndex' in curr_conditions['currently'] and isFloat(curr_conditions['currently']['uvIndex']):
                streamer.log(":sunny: UV Index:",curr_conditions['currently']['uvIndex'])

            if 'summary' in curr_conditions['currently']:
                streamer.log(":cloud: Weather Summary",curr_conditions['currently']['summary'])

            if 'hourly' in curr_conditions:
                streamer.log("Today's Forecast",curr_conditions['hourly']['summary'])

            if 'daily' in curr_conditions:
                if 'data' in curr_conditions['daily']:
                    if 'moonPhase' in curr_conditions['daily']['data'][0]:
                        moon_phase = curr_conditions['daily']['data'][0]['moonPhase']
                        streamer.log(":crescent_moon: Moon Phase",moon_icon(moon_phase))
                        streamer.log(":cloud: Weather Conditions",weather_status_icon(curr_conditions['currently']['icon'],moon_phase))

            streamer.flush()
        time.sleep(60*MINUTES_BETWEEN_READS)

if __name__ == "__main__":
    main()