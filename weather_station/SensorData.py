'''
Created on Jan 26, 2019

@author: Kedar Patil
'''

import os
from datetime import datetime

class SensorData(object):
    
    '''
    @param timeStamp: current date & time 
    @param minVal: min. temperature data
    @param maxVal: max. temperature data
    @param currentVal: current temperature value
    @param avgVal: average temperate data
    @param totalVal: total temperature value
    @param samples: gives all the samples count as soon as the alert message is sent out
    '''
    
    timeStamp = None
    minVal = 30
    maxVal = 0
    currentVal = 0
    avgVal = 0
    totalVal = 0
    samples = 0
    
    def __init__(self):
        self.timeStamp = str(datetime.now()) #getting current time & date
        
    def addVal(self, newVal):
        self.samples += 1         #when a new value is detected, increase the sample by 1
        self.timeStamp = str(datetime.now())
        self.currentVal = newVal  #setting the new value as the current value
        self.totalVal += newVal   #Adding new value to total value
        
        '''
        checking a condition to set the min. value
        '''
        
        if (self.currentVal < self.minVal):
            self.minVal = self.currentVal
            
        '''
        checking a condition to set the max. value
        '''
        
        if (self.currentVal > self.maxVal):
            self.maxVal = self.currentVal
         
        '''
        checking a condition to set the average value
        '''
           
        if (self.totalVal != 0 and self.samples > 0):
            self.avgVal = self.totalVal / self.samples
     
    '''
    getting and returning the min. value
    '''
    
    def getMinValue(self):
        return self.minVal
    
    '''
    getting and returning the max. value
    '''
    
    def getMaxValue(self):
        return self.maxVal
    
    '''
    getting and returning the average value
    '''
            
    def getAvgValue(self):
        return self.avgVal
    
    '''
    getting and returning the current value
    '''
    
    def getValue(self):
        return self.currentVal
    
    '''
    Returns a string representation of  parameters from TempSensorEmulator
    '''
    
    def __str__(self):
        customStr = \
        str(
        os.linesep + '\tTime: ' + self.timeStamp + \
        os.linesep + '\tMin: ' + str(round(self.minVal, 3)) + \
        os.linesep + '\tMax: ' + str(round(self.maxVal, 3)) + \
        os.linesep + '\tCurrent: ' + str(round(self.currentVal, 3)) + \
        os.linesep + '\tAverage: ' + str(round(self.avgVal, 3)) + \
        os.linesep + '\tSamples: ' + str(self.samples))
        return customStr