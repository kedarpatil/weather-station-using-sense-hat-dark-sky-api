'''
Created on Jan 26, 2019

@author: Kedar Patil
'''

import configparser
import os
import ConfigConst

''' 
Creating a class to load and get the data from configuration file
'''

class ConfigUtil(object):
    configData = None
    isLoaded = False
    configConst = None
    
    def __init__(self):
        self.configConst = ConfigConst.ConfigConst()
        self.configData = configparser.ConfigParser()
        self.configFilePath = self.configConst.DEFAULT_CONFIG_FILE_NAME
    
    '''
    Creating a method to load the configuration data from config file 
    stored at config path
    '''
            
    def loadConfig(self):
        
        if(os.path.exists(self.configFilePath)):
            self.configData.read(self.configFilePath)
            self.isLoaded = True
    
    '''
    Creating a method to get all the config data
    '''
                  
    def getConfigData(self, forceReload=False):
        
        if(self.isLoaded == False or forceReload): #checking if the config data has loaded
            self.loadConfig()
            
        return self.configData
    
    '''
    Creating a method to get the config file and return the path
    where it is stored
    '''
    
    def getConfigFile(self):
        return self.configFilePath
    
    '''
    fetching relevant key values from ConfigConst
    '''
    
    def getProperty(self, section, key):
        return self.getConfigData(forceReload=False).get(section, key)
    
    '''
    Creating a method to check if the configured data is loaded
    and if it is, then return a boolean value
    '''

    def isConfiguredDataLoaded(self):
        return self.isLoaded