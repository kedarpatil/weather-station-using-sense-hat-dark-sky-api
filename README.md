# README #

Semester project for connected devices CSYE6530

This is a complete cloud-based IoT solution created using the Raspberry Pi and the Sense HAT shield.

Local weather data is collected form the sensors on the Sense HAT and this data is combined 
with the dark sky API which provides the location based weather data.

This device is connected to ubidots for cloud integration and initialstate for data visualization.
Node-RED is used to create a flow that connects the device to cloud using MQTT protocol.

Device alerts are received via email or text messages.